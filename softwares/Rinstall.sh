#!/bin/bash
#===============================================================================
#
#          FILE:  Rinstall.sh
# 
#         USAGE:  sudo ./Rinstall.sh 
# 
#   DESCRIPTION:  used to install R and everything about it I commonly need
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:   (), 
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  05/02/2015 12:04:07 CET
#      REVISION:  ---
#===============================================================================
alreadyCRAN=$(grep "cran" /etc/apt/sources.list)

if [[ $alreadyCRAN == "" ]] ; then
    echo "deb http://cran.rstudio.com/bin/linux/ubuntu trusty/" >> /etc/apt/sources.list
    echo "deb http://cran.rstudio.com/bin/linux/ubuntu trusty/" >> /etc/apt/sources.list
fi
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9
sudo add-apt-repository -y ppa:ubuntugis/ubuntugis-unstable

apt-get update 
apt-get install -y r-base 
apt-get install -y r-cran-rcurl 
apt-get install -y libgdal-dev 
apt-get install -y libproj-dev 
# apt-get install -y opencpu-full

# install Rstudio
sudo apt-get install -y libjpeg62
sudo dpkg -i /vagrant_data/rstudio-0.98.1103-amd64.deb

# default source in R
echo 'options(repos=structure(c(CRAN="http://cran.rstudio.com/")))' >> ~/.Rprofile

Rscript -e 'install.packages(c("rgdal","zoom","rgeos","devtools","roxygen2","testthat","plotrix","spam"))'

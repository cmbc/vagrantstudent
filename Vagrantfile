# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/trusty64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder "data", "/vagrant_data"
  config.vm.synced_folder "softwares", "/vagrant_softwares"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  
  # VirtualBox:
  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true
  
    # Customize the amount of memory on the VM:
    vb.memory = "2048"
    vb.cpus = 2
    vb.customize ["modifyvm", :id, "--cpuexecutioncap", "90","--vram","32"]
  end

  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-SHELL
    # add swap file
    sudo swapoff -a # off if exist already
    sudo rm -f /swapfile # remove if existing

    sudo fallocate -l 2G /swapfile
    sudo chmod 600 /swapfile
    sudo mkswap /swapfile
    sudo swapon /swapfile

    # add the swap to fstab
    initFile=$(grep -v "swapfile" /etc/fstab)
    sudo mv /etc/fstab /etc/fstab.bak
    echo -e "$initFile \n/swapfile   none    swap    sw    0   0" > /tmp/fstab
    sudo mv /tmp/fstab /etc/fstab

    # update the system and install the gui
    sudo apt-get update
    sudo apt-get dist-upgrade
    sudo apt-get install -y ubuntu-desktop
    sudo apt-get install -y language-pack-fr git 
    fileToModify="/etc/X11/Xwrapper.config"
    sudo cp $fileToModify $fileToModify.bak
    tmpfile="/tmp/tempfile"
    sed -e "s/^allowed_users=.*/allowed_users=all/" $fileToModify > $tmpfile
    sudo mv $tmpfile $fileToModify

    # linux tools
    sudo apt-get install -y tmux terminator subversion
    sudo apt-get install -y trash-cli vim-gtk diodon meld

    # set the time zone 
    TIMEZONE="Europe/Paris"      
    echo $TIMEZONE > /etc/timezone                     
    sudo cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime   # This sets the time

    # Qgis
    gpg --keyserver keyserver.ubuntu.com --recv DD45F6C3
    gpg --export --armor DD45F6C3 | sudo apt-key add -
    sudo apt-get install python-software-properties
    sudo add-apt-repository -y ppa:ubuntugis/ubuntugis-unstable
    sudo apt-get install -y qgis python-qgis qgis-plugin-grass

    # install google earth
    sudo dpkg --add-architecture i386
    sudo apt-get update
    sudo sudo apt-get install -y libc6-i386 libglib2.0-0:i386 libsm6:i386 \
    libglu1-mesa:i386 libgl1-mesa-glx:i386 libxext6:i386 \
    libxrender1:i386 libx11-6:i386 libfontconfig1:i386 lsb-core
    # the version 7, display a bit garbled
    # sudo dpkg -i -y /vagrant_softwares/google-earth-stable_current_i386.deb
    sudo dpkg -i /vagrant_softwares/googleearth_6.2.2.6613-r0_i386.deb

  SHELL
  config.vm.provision :shell, path: "softwares/Rinstall.sh"
end
